# Presubmission Scaffold

Gerüst für das Aufsetzen eines Projekts zum Presubmission Feedback.

## Erste Schritte

- [ ] Anlegen eines ersten Issues mit dem Titel "Einrichtung des Beitrags für Presubmission Feedback" o.ä.  
  Ein entsprechendes Template kann beim Anlegen des Issues ausgewählt werden. Zuweisung erfolgt an den/die Presubmission Facilitator.