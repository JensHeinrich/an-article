# Checkliste für Presubmission Feedback

## Vorbesprechung

- [ ] Datenschutzaspekte mit Autor_innen klären

## Projekteinrichtung in GitLab

- [ ] Anlegen eines GitLab-Projektes zur Aufnahme der ersten Textfassung mit aussagekräftiger Beschreibung als privates Projekt. Hierfür kann [ein Gerüst in GitLab](https://collaborating.tuhh.de/hos/modernes-publizieren/offen/software/konfigurationen/presubmission-scaffold) importiert werden
- [ ] Anlegen eines ersten Issues mit dem Titel "Einrichtung des Beitrags" mit dieser Checkliste als Template
- [ ] Zuweisung des Issues an den/die Presubmission Facilitator
- [ ] Konfiguration des Projekts über **Settings > General > Permissions**:
    - [ ] Sichtbarkeit: privat
    - [ ] Issues
    - [ ] Repository mit Merge Requests und Pipelines
- [ ] Zuweisen eines (Shared) Runners über **Settings > CI/CD > Runners**
- [ ] Ablegen der originalen Textfassung(en) im Ordner `delivery`.

## Einrichtung von hypothes.is

- [ ] Anlegen einer projektbezogenen privaten Gruppe in hypothes.is. Diese wird später für den Reviewprozess benutzt.

## Projekteinrichtung in GitLab

- [ ] ~~Aktualisierung der Gruppen-ID in der Datei `assets/hypothesis.html`~~
- [ ] Definition der *Runner Variable* `REVIEW_FEEDBACK_NAMESPACE` mit einem achtstelligen Hash (z.B. `a42DbO1Y`) über **Settings > CI/CD > Environment Variables**
- [ ] Generieren einer Kombination von Benutzername und Passwort für die Datei `.htpasswd`, um den Zugriff auf die HTML-Dokumente im Review einzuschränken. Hierfür kann ein Onlinetool genutzt werden wie <http://www.htaccesstools.com/htpasswd-generator/>
- [ ] Eintragen der generierten Zeichenkette in die Datei `.htpasswd` (Standardzugang ist: `foo:bar`).

## Textaufbereitung in GitLab oder im lokalen Editor

- [ ] Konvertieren und Einpflegen der ersten Textfassung als `draft.md`.
- [ ] Hinzufügen und verlinken von Abbildungen
- [ ] Prüfung der Zitation und Referenzliste

## Projekteinrichtung in GitLab

- [ ] Anlegen des Branches `01-iteration` auf Basis des aktuellen `masters`. Diese erste Fassung wird zum Review bereitgestellt. Der `master` ist für die finale Version reserviert.

## Prüfung im Browser

- [ ] Aufruf der Vorschau vom Branch `01-iteration` über **Operations > Environments**. Hier sind Benutzername und Passwort erforderlich.
- [ ] Prüfung der Vorschau im Browser
    - [ ] Sind die Metadaten korrekt dargestellt (Autor_in, Institution etc.)?
    - [ ] Ist die optische Darstellung korrekt?
    - [ ] Funktioniert der Passwortschutz?

## Projekteinrichtung in GitLab (Fortsetzung)

- [ ] Erstellen des Branches `02-iteration` auf Basis von `01-iteration`, damit die Autor_innen das Feedback aus dem Review schon einarbeiten können, das zu `01-iteration` gegeben wird.
- [ ] Festlegung des Branches `02-iteration` als *Default Branch*.
- [ ] Einladen des/der Autor_innen in das GitLab-Projekt über **Settings > Members**.

## Issue in GitLab

- [ ] Anlegen eines weiteren Issues in GitLab mit dem Template "Briefing".
- [ ] Zuweisung des Issues an den/die primäreN Autor_in.
- [ ] ggf. gesonderte Benachrichtigung mit Hinweis auf dieses Issues
