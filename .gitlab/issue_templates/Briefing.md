Liebe Autorin, lieber Autor!

Ihr Beitrag ist nun in das *Pre-submission Feedback System* eingepflegt. In den folgenden Absätzen möchten wir das empfohlene weitere Vorgehen beschreiben:

## Eintreten in die Hypothesis-Gruppe

Für das Feedback zu Ihrem Beitrag wurde eine private Gruppe in Hypothesis angelegt. Diese ist für Ihren Beitrag voreingestellt, sodass Anmerkungen nur in dieser Gruppe gespeichert werden.

**Treten Sie dieser privaten Gruppe nun über den Einladungslink [Hier-die-Einladungs-URL-von-Hypothesis] bei.** [Dafür ist ein Account bei Hypothesis notwendig](https://hypothes.is/signup).

## Überprüfen der Beitragsvorschau im Browser

Die erste Iteration des Beitrags kann nun unter der URL [Hier-der-Link-zu-01-iteration] angesehen und kommentiert werden. Schalten Sie den Zugriffsschutz mit dem Benutzernamen **[.htpasswd-Benutzername]** und das Passwort **[.htpasswd-Passwort]** frei.

Bitte prüfen Sie, ob alle Daten korrekt aus Ihrem Beitrag übernommen wurden. Anmerkungen und Fragen können Sie unter dieses Issue schreiben.

## Annotieren und kommentieren mit Hypothesis

Wählen Sie oben in dem Seitenpanel, das von Hypothesis geöffnet wird, die private Gruppe aus, in der Sie den Beitrag begutachten wollen.

![Auswahl der privaten Gruppe](../../assets/hypothesis-choose-group.png)

*Hinweis: Ggf. zeigt das Panel, dass Sie noch nicht eingeloggt sind, obwohl Sie das vorher schon erledigt haben. Klicken Sie in diesem Fall auf "Log in".*

## Benachrichtigen Ihrer Community

Wenn Sie mit der Version einverstanden sind,

- [ ] Versenden Sie den Link [Hier-der-Link-zu-01-iteration] zur Beitragsvorschau.
- [ ] Laden Sie Ihre Community mit dem Link [Hier-die-Einladungs-URL-von-hypothes.is] in die private Hypothesis-Gruppe ein.
- [ ] Geben Sie Benutzername und Passwort für das Freischalten der Seite weiter.
- [ ] Setzen Sie ggf. eine Frist für das Feedback zu Ihrem Beitrag.
- [ ] Weisen Sie darauf hin, dass der Beitrag nur im Netz der TUHH erreichbar ist!

## Einarbeiten der Anmerkungen

Arbeiten Sie Änderungen direkt in die Datei `draft.md` unter [Hier-der-Link-zu-02-iteration-draft.md] in GitLab ein. Das System ist so eingestellt, dass sich Änderungen an der Datei nicht auf die Vorschau für Ihre Community auswirken. Das bedeutet, dass Sie auch schon vor Ablauf einer Feedbackfrist Änderungen einpflegen können.

Eine Vorschau dieser zweiten Fassung Ihres Beitrags ist unter dem Link [Hier-der-Link-zu-02-iteration] zu sehen. 

## Fragen?

Bitte schreiben Sie alle Fragen und Anmerkungen in das Textfeld unter diesem Briefing.